<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->group(['prefix' => 'weather'], function ($router) {
    $router->post('forecastio', 'Weather\ForecastIo@run');
});

$router->group(['prefix' => 'calc'], function ($router) {
    $router->post('wolfram', 'Calc\WolframAlpha@run');
});

$router->group(['prefix' => 'np'], function ($router) {
    $router->post('ds', 'NowPlaying\Darkscience@run');
});

$router->group(['prefix' => 'timezone'], function ($router) {
    $router->post('tz', 'Timezone\Timezone@run');
});

$router->group(['prefix' => 'horoscope'], function ($router) {
    $router->post('today', 'Horoscope\Horoscope@run');
});

$router->group(['prefix' => 'quotes'], function ($router) {
    $router->group(['prefix' => 'darchoods'], function ($router) {
        $router->post('get', 'Quotes\Darchoods@getQuote');
        $router->post('add', 'Quotes\Darchoods@addQuote');
    });
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->get('weather', 'Api\ApiController@weather');
    $router->post('weather', 'Api\ApiController@weather');

    $router->get('calc', 'Api\ApiController@calc');
    $router->post('calc', 'Api\ApiController@calc');

    $router->get('np', 'Api\ApiController@np');

    $router->get('tz', 'Api\ApiController@tz');
    $router->get('hs', 'Api\ApiController@hs');
});
