<?php

use GuzzleHttp\Middleware;

/** goutte helper **/
function getNode($request, $selector, $default = null)
{
    return $request->filter($selector)->count() ? strip_whitespace($request->filter($selector)->first()->text()) : $default;
}

function goutteClient()
{
    $client = new \Goutte\Client();
    // $client->getClient()->setDefaultOption('config', ['curl' => ['CURLOPT_TIMEOUT' => 2]]);

    return $client;
}

function goutteRequest(\Goutte\Client $client, $url, $method = 'get')
{
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
        return false;
    }

    try {
        $request = $client->request(strtoupper($method), $url);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        return $e->getMessage();
        return -1;
    } catch (\GuzzleHttp\Exception\ServerException $e) {
        return $e->getMessage();
        return -2;
    } catch (ErrorException $e) {
        return $e->getMessage();
        return -3;
    } catch (\Exception $e) {
        return $e->getMessage();
        return -4;
    }

    if (($request instanceof \Symfony\Component\DomCrawler\Crawler) === false) {
        return -5;
    }

    // if ($request->getStatusCode() != 200) {
    //     return -3;
    // }

    return $request;
}

function guzzle($method, $url, $data = [], $client = null)
{
    if (!filter_var($url, FILTER_VALIDATE_URL)) {
        return false;
    }

    if (count($data)) {
        if ($method == 'post') {
            $data = ['form_params' => $data];
        } else {
            $data = ['body' => $data];
        }
    }
    $data['http_errors'] = false;

    // Create a middleware that echoes parts of the request.
    $tapMiddleware = Middleware::tap(function ($request) {
        echo $request->getHeaderLine('Content-Type');
        // application/json
        echo $request->getBody();
        // {"foo":"bar"}
    });

    try {
        if ($client === null || ($client instanceof \GuzzleHttp\Client) === null) {
            $client = new \GuzzleHttp\Client();
        } else {
            $client = new $client;
        }

        //$data['handler'] = $tapMiddleware($client->getConfig('handler'));

        $response = $client->request($method, $url, $data);
    } catch (\GuzzleHttp\Exception\ClientException $e) {
        return $e->getMessage();
        return -1;
    } catch (\GuzzleHttp\Exception\ServerException $e) {
        return $e->getMessage();
        return -2;
    } catch (ErrorException $e) {
        return $e->getMessage();
        return -3;
    } catch (\Exception $e) {
        return $e->getMessage();
        return -4;
    }

    //if ($response->getStatusCode() != '200') {
    //    return -5;
    //}

    return $response;
}

function strip_whitespace($msg)
{
    $msg = str_replace(["\n", "\r\n", "\r", "\t"], ' ', $msg);
    $msg = trim(preg_replace('/\s+/', ' ', $msg));
    return $msg;
}
