<?php

namespace App\Http\Controllers\Calc;

use Illuminate\Http\Request;

class WolframAlpha extends BaseCalc
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\Calc\WolframAlpha';
    protected $version = '1.0.1';
    protected $services = ['wolframalpha'];

    public function run()
    {
        if (array_get($this->request, 'message.arg_count', '0') == '0' ||
            substr(array_get($this->request, 'message.args.0'), 0, 1) == '?') {
            return $this->sendResponse('Usage: <calculation query>', 200);
        }

        $data = $this->getData();
        if (is_array($data) && isset($data['status'])) {
            return $this->sendResponse($data['message'], $data['status']);
        }

        if (empty($data)) {
            $data = [
                'Error: No Results Found.',
            ];
        }

        return $this->sendResponse('ok', '200', [
            'raw' => $data,
            'return' => [
                'to' => array_get($this->request, 'message.to'),
                'method' => 'privmsg',
                'message' => implode(' // ', $data),
            ],
        ]);
    }

    private function getData()
    {
        $url = 'http://api.wolframalpha.com/v2/query?' . http_build_query([
            'input' => array_get($this->request, 'message.text', '0'),
            'appid' => array_get($this->request, 'services.wolframalpha'),
        ]);

        // grab the request
        $request = goutteRequest(goutteClient(), $url, 'get');
        if (($request instanceof \Symfony\Component\DomCrawler\Crawler) === false) {
            return [
                'status' => 400,
                'message' => 'Error: Could not query the server.',
            ];
        }

        // setup some sane defaults to check for
        $return = [];
        $results = [
            '0' => $request->filterXPath('//pod[@id="Result"]/subpod'),
            '1' => $request->filterXPath('//pod/subpod'),
        ];

        foreach ($results as $key => $result) {
            // if we get a InvalidArgumentException, this one failed, continue over it
            try {
                $text = $result->text();
            } catch (\InvalidArgumentException $e) {
                continue;
            } catch (ErrorException $e) {
                continue;
            } catch (Exception $e) {
                continue;
            }

            // process it and pass results back to $return
            $text = strip_whitespace($text);
            if (strpos("\n", $text) !== false) {
                $return[] = $text;
            } else {
                $lines = explode("\n", $text);
                foreach ($lines as $text) {
                    $return[] = $text;
                }
            }
        }

        return $return;
    }
}
