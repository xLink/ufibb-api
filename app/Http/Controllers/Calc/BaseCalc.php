<?php

namespace App\Http\Controllers\Calc;

use App\Http\Controllers\Controller;

class BaseCalc extends Controller
{
    protected $commands = ['calc'];
}
