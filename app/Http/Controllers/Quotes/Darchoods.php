<?php

namespace App\Http\Controllers\Quotes;

use Illuminate\Http\Request;

class Darchoods extends BaseQuotes
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\Quotes\Darchoods';
    protected $version = '1.0.1';
    protected $services = ['darchoods'];

    public function getQuote()
    {
        // make sure we have something for this request
        if (substr(array_get($this->request, 'message.args.0'), 0, 1) == '?') {
            return $this->sendResponse('Usage: <id number> || null(random quote)', 200);
        }

        $quote = $this->getQuoteData();
        if (is_array($quote) && isset($quote['status'])) {
            return $this->sendResponse($quote['message'], $quote['status']);
        }

        $raw = [
            sprintf('Quote#%s', array_get($quote, 'quote_id', '0')),
            array_get($quote, 'content', null),
        ];

        return $this->sendResponse('ok', '200', [
            'raw' => $raw,
            'return' => [
                'to' => array_get($this->request, 'message.to'),
                'method' => 'privmsg',
                'message' => implode(': ', $raw),
            ],
        ]);
    }

    public function addQuote()
    {
        // make sure we have something for this request
        if (array_get($this->request, 'message.arg_count', '0') == '0' ||
            substr(array_get($this->request, 'message.args.0'), 0, 1) == '?') {
            return $this->sendResponse('Usage: <msg to quote>', 200);
        }

        $url = 'https://www.darchoods.net/api/qdb/create';
        $request = guzzle('post', $url, [
            'channel' => array_get($this->request, 'message.to'),
            'author' => array_get($this->request, 'sender.nick'),
            'quote' => array_get($this->request, 'message.text', '0'),
        ], '\GuzzleHttp\DarchoodsClient');

        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error: Could not query the server.',
            ];
        }

        if ($request->getStatusCode() != '200') {
            return [
                'status' => 400,
                'message' => 'Error: QDB appears to be down, Try again later.',
            ];
        }

        $data = $request->json();
        $raw = [
            'Thank you for your submission.',
            sprintf('Your quote has been added as number %d', array_get($data, 'data.quote.quote_id', 0)),
        ];

        return $this->sendResponse('ok', '200', [
            'raw' => $raw,
            'return' => [
                'to' => array_get($this->request, 'message.to'),
                'method' => 'privmsg',
                'message' => implode(' ', $raw),
            ],
        ]);
    }

    private function getQuoteData()
    {
        $quote_id = array_get($this->request, 'message.args.0');
        if (empty($quote_id)) {
            $quote_id = 0;
        }

        // figure out if specific or random quote
        $url = 'https://www.darchoods.net/api/qdb/random';
        if (array_get($this->request, 'message.arg_count', '0') != '0') {
            $url = 'https://www.darchoods.net/api/qdb/search/byId';
        }

        $request = guzzle('post', $url, [
            'channel' => array_get($this->request, 'message.to'),
            'quote_id' => $quote_id,
        ], $this->getClient());

        if (($request instanceof \GuzzleHttp\Message\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error: Could not get quote. Does this channel have any?',
            ];
        }

        if ($request->getStatusCode() != '200') {
            return [
                'status' => 400,
                'message' => 'Error: QDB appears to be down, Try again later.',
            ];
        }

        $data = $request->json();
        $quote = array_get($data, 'data.quote');
        if ($quote == false) {
            return [
                'message' => 'Error: Either Quote wasnt found or there are no quotes in this channel.',
                'status' => 500,
            ];
        }

        return $quote;
    }
}
