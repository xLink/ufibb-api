<?php

namespace App\Http\Controllers\Quotes;

use App\Http\Controllers\Controller;

class BaseQuotes extends Controller
{
    protected $commands = ['quote'];
}
