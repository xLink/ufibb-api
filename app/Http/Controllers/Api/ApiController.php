<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function weather(Request $input)
    {
        $location = $input->get('location', 'dy2');

        // die(var_dump($location));
        $jsonRequest = [
            'message' => [
                'to' => '#bots',
                'command' => '>w',
                'arg_count' => '1',
                'args' => [
                    $location,
                ],
                'text' => $location,
                'raw' => ':>w ' . $location,
                'timestamp' => time(),
            ],
            'sender' => [
                'nick' => 'xLink',
                'ident' => 'sid3260',
                'mask' => 'staff.darkscience.net',
                'full' => 'xLink!sid3260@staff.darkscience.net',
            ],
            'server' => 'irc.darkscience.net',
            'services' => [
                'forecastio' => env('API_FORECASTIO'),
            ],
        ];

        // try {
        $request = guzzle('post', 'http://ufibb.cybershade.org/weather/forecastio', $jsonRequest);

        // } catch (\Exception $e) {
        //     return $e->getMessage();
        // }

        return array_get(json_decode($request->getBody(), true), 'data');
    }

    public function calc(Request $input)
    {
        $query = $input->get('query', '1 year in days');

        // die(var_dump($query));
        $args = explode($query, ' ');
        $jsonRequest = [
            'message' => [
                'to' => '#bots',
                'command' => '>calc',
                'arg_count' => count($args),
                'args' => $args,
                'text' => $query,
                'raw' => ':>calc ' . $query,
                'timestamp' => time(),
            ],
            'sender' => [
                'nick' => 'xLink',
                'ident' => 'sid3260',
                'mask' => 'staff.darkscience.net',
                'full' => 'xLink!sid3260@staff.darkscience.net',
            ],
            'server' => 'irc.darkscience.net',
            'services' => [
                'wolframalpha' => env('API_WOLFRAM'),
            ],
        ];

        // try {
        $request = guzzle('post', 'http://ufibb.cybershade.org/calc/wolfram', $jsonRequest);

        // } catch (\Exception $e) {
        //     return $e->getMessage();
        // }

        return array_get(json_decode($request->getBody(), true), 'data');
    }

    public function np(Request $input)
    {
        $query = null;
        $args = [];
        $jsonRequest = [
            'message' => [
                'to' => '#bots',
                'command' => '>np',
                'arg_count' => count($args),
                'args' => $args,
                'text' => $query,
                'raw' => ':>np ' . $query,
                'timestamp' => time(),
            ],
            'sender' => [
                'nick' => 'xLink',
                'ident' => 'sid3260',
                'mask' => 'staff.darkscience.net',
                'full' => 'xLink!sid3260@staff.darkscience.net',
            ],
            'server' => 'irc.darkscience.net',
            'services' => [],
        ];

        try {
            $request = guzzle('post', 'http://ufibb.cybershade.org/np/ds', $jsonRequest);

        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return array_get(json_decode($request->getBody(), true), 'data');
    }

    public function tz(Request $input)
    {
        $query = $input->get('location', 'dy2');

        $jsonRequest = [
            'message' => [
                'to' => '#bots',
                'command' => '>tz',
                'arg_count' => 1,
                'args' => [$query],
                'text' => $query,
                'raw' => ':>tz ' . $query,
                'timestamp' => time(),
            ],
            'sender' => [
                'nick' => 'xLink',
                'ident' => 'sid3260',
                'mask' => 'staff.darkscience.net',
                'full' => 'xLink!sid3260@staff.darkscience.net',
            ],
            'server' => 'irc.darkscience.net',
            'services' => [
                'google' => env('API_GOOGLE'),
            ],
        ];

        try {
            $request = guzzle('post', 'http://ufibb.cybershade.org/timezone/tz', $jsonRequest);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        // var_dump($request);die;
        return array_get(json_decode($request->getBody(), true), 'data');
    }

    public function hs(Request $input)
    {
        $query = $input->get('sign', 'virgo');

        $jsonRequest = [
            'message' => [
                'to' => '#bots',
                'command' => '>tz',
                'arg_count' => 1,
                'args' => [$query],
                'text' => $query,
                'raw' => ':>hs ' . $query,
                'timestamp' => time(),
            ],
            'sender' => [
                'nick' => 'xLink',
                'ident' => 'sid3260',
                'mask' => 'staff.darkscience.net',
                'full' => 'xLink!sid3260@staff.darkscience.net',
            ],
            'server' => 'irc.darkscience.net',
            'services' => [],
        ];

        try {
            $request = guzzle('post', 'http://ufibb.cybershade.org/horoscope/today', $jsonRequest);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        // var_dump($request);die;
        return array_get(json_decode($request->getBody(), true), 'data');
    }
}
