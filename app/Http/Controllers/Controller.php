<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Http\ResponseFactory;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected $class = null;
    protected $author = null;
    protected $version = null;
    protected $commands = [];
    protected $services = [];
    protected $request = [];

    public $response;

    public function __construct(Request $request, ResponseFactory $response)
    {
        $this->response = $response;
        $this->request = $request->all();

        // process the json
        if (($message = $this->processJsonBody($request)) === false) {
            return $this->sendError('Error: Malformed JSON Sent', 412);
        }

        // make sure all keys are present
        if (($message = $this->jsonKeysPresent()) !== true) {
            return $this->sendError($message, 412);
        }

        // check to make sure requested service is present
        if (($message = $this->checkForServices()) !== true) {
            return $this->sendError($message, 412);
        }
    }

    private function processJsonBody(Request $request)
    {
        $body = $request->getContent();

        $body = stripslashes($body);
        // $body = preg_replace('/\s\s+/', ' ', $body);
        $body = json_decode($body, true);

        if (is_null($body)) {
            return false;
        }

        $this->request = $body;

        return true;
    }

    private function jsonKeysPresent()
    {
        $keys = [
            'message',
            'message.to',
            'message.command',
            'message.arg_count',
            'message.text',
            'message.args',
            'message.raw',
            'message.timestamp',
            'sender',
            'sender.nick',
            'sender.ident',
            'sender.mask',
            'sender.full',
            'server',
            'services',
        ];

        foreach ($keys as $key) {
            if (array_get($this->request, $key, false) === false) {
                return 'Error: Request ' . $key . ' is not specified';
            }
        }

        return true;
    }

    private function checkForServices()
    {
        // dont need to carry on if this command doesnt want services
        if (empty($this->services)) {
            return true;
        }

        // if it does, we better make sure we specify em
        if (empty(array_get($this->request, 'services', []))) {
            return 'Error: No Services Specified';
        }

        // loop over the services the cmd expects, and make sure we have specified em
        foreach ($this->services as $service) {
            if (!in_array($service, array_get($this->request, 'services', []))) {
                return 'Error: Missing ' . $service . ' API Key';
            }

            if (empty(array_get($this->request, 'services.' . $service, []))) {
                return 'Error: ' . $service . ' API Key is empty';
            }
        }

        return true;
    }

    private function getClassUpdatedTimestamp()
    {
        $class = $this->class;
        if ($class === null) {
            return 0;
        }

        $class = str_replace('\\', '/', $class) . '.php';
        $class = str_replace('App', 'app', $class);

        return filemtime(base_path($class));
    }

    /**
     * Alias method for sending an response back.
     *
     * @param string $message
     * @param int    $status  HTTP Status Code
     * @param array  $data
     */
    public function sendResponse($message = 'ok', $status = 200, $data = [])
    {
        $reply = [
            'status' => $status,
            'message' => $message,
            'timestamp' => time(),
            'api' => [
                'version' => $this->version,
                'author' => $this->author,
                'services' => $this->services,
                'commands' => $this->commands,
                'last_updated' => $this->getClassUpdatedTimestamp(),
            ],
        ];

        if (!empty($data)) {
            $reply['data'] = $data;
        }

        if ($this->request) {
            // die(var_dump($this->request));
            $reply['request'] = $this->request;
        }

        return response()->json($reply, $status);
    }

    /**
     * Alias method for sending an error status back.
     *
     * @param string $message
     * @param int    $status  HTTP Status Code
     */
    public function sendError($message, $status = 500)
    {
        return $this->sendResponse($message, $status);
    }

    /**
     * Alias method for sending an ok status back.
     *
     * @param string $message
     * @param int    $status  HTTP Status Code
     */
    public function sendOK($message, $status = 200)
    {
        return $this->sendResponse($message, $status);
    }

    /**
     * On missing method, throw an error.
     *
     * @param array $parameters
     */
    public function missingMethod($parameters = [])
    {
        return $this->sendError('Invalid Method');
    }
}
