<?php

namespace App\Http\Controllers\Timezone;

use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTimeZone;

class Timezone extends BaseTz
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\Timezone\Timezone';
    protected $version = '1.0.4';
    protected $services = ['google'];

    protected $cache;
    protected $cacheKey;
    protected $cacheHit;

    public function run(Request $request, Cache $cache)
    {
        $input = $request->all();
        $this->cache = $cache;

        // make sure we have something for this request
        if (substr(array_get($input, 'message.args.0'), 0, 1) == '?') {
            return $this->sendError('Usage: <location>', 200);
        }

        // check for weather info out of darchoods api
        if (array_get($input, 'message.arg_count', '0') == '0') {
            return $this->sendError('Usage: <location>. (Sorry you\'ll have to ask for location, I can\'t access the DH Api yet)', 400);
        }

        // grab the long/lat for the cmd params
        $location = str_replace(array_get($input, 'message.command').' ', '', array_get($input, 'message.text'));
        $this->cacheKey = 'longLat-'.$location;
        $longLat = $this->getGoogleLongLat($location, $request);
        // var_dump($longLat);die;
        if (is_array($longLat) && !isset($longLat['results'])) {
            clock('issue with longLat, removing cache key');
            $this->cache->forget($this->cacheKey);
            return $this->sendError($longLat['message'], $longLat['status']);
        }

        // grab the actual forcast for the location
        $timezone = $this->getTimezone($longLat, $request);
        // var_dump($timezone);die;
        if (is_array($timezone) && !isset($timezone['timeZoneId'])) {
            return $this->sendError($timezone['message'], $timezone['status']);
        }

        $return = $this->formatOutput($timezone, $longLat, $request);
        return $this->sendResponse('ok', '200', $return);
    }

    private function formatOutput($timezone, $location, Request $request)
    {
        $input = $request->all();
        $timezoneId = array_get($timezone, 'timeZoneId');

        $carbon = Carbon::now(new DateTimeZone(array_get($timezone, 'timeZoneId')));

        $raw = [
            'location' => array_get($location, 'results.0.formatted_address'),
            'raw_offset' => array_get($timezone, 'rawOffset'),
            'dst_offset' => array_get($timezone, 'dstOffset'),
            'timezone_id' => $timezoneId,
            'timezone_name' => array_get($timezone, 'timeZoneName'),
            'time_formatted' => $carbon->toCookieString(),
            'time_raw' => $carbon->format('U'),
            'timezone_offset' => $carbon->format('O'),
        ];

        return [
            'raw' => $raw,
            'return' => [
                'to' => array_get($input, 'message.to'),
                'method' => 'privmsg',
                'message' => sprintf(
                    '[ %s | %s | %s | %s | %s ]',
                    array_get($raw, 'location'),
                    array_get($raw, 'timezone_id'),
                    array_get($raw, 'timezone_name'),
                    array_get($raw, 'time_formatted'),
                    array_get($raw, 'timezone_offset')
                ),
            ],
        ];
    }

    private function getGoogleLongLat($location, Request $request)
    {
        $input = $request->all();
        if (empty($location)) {
            return [
                'status' => 411,
                'message' => 'Error: No Location Given.',
            ];
        }

        if ($this->cache->has($this->cacheKey)) {
            clock('got longLat returning');
            $this->cacheHit = true;
            return $this->cache->get($this->cacheKey);
        }
        clock('no longLat data');
        $this->cacheHit = false;

        $url = sprintf(
            'https://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false&key=%s',
            urlencode($location),
            array_get($input, 'services.google')
        );
        clock('pinging', $url);
        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error: Could not query the server.',
            ];
        }

        $gAPI = json_decode($request->getBody(), true);
        if (!count($gAPI)) {
            return [
                'status' => 400,
                'message' => 'Error 1: Location seems to be invalid, try again.',
            ];
        }

        if (in_array(array_get($gAPI, 'status'), ['OVER_QUERY_LIMIT'])) {
            return [
                'status' => 400,
                'message' => 'Error: Query limited reached for key. Please wait a while.',
            ];
        }

        $this->cache->rememberForever($this->cacheKey, function () use ($gAPI) {
            clock('adding longLat to cache');
            return $gAPI;
        });

        return $gAPI;
    }

    private function getTimezone($location, Request $request)
    {
        $input = $request->all();
        if (!is_array($location)) {
            return [
                'status' => 411,
                'message' => 'Error: Could not get long/lat for location.',
            ];
        }

        $longLat = array_get($location, 'results.0.geometry.location');
        $longLat = (string) $longLat['lat'] . ',' . $longLat['lng'];

        $url = sprintf(
            'https://maps.googleapis.com/maps/api/timezone/json?location=%s&timestamp=%d&key=%s',
            $longLat,
            time(),
            array_get($input, 'services.google')
        );
        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 411,
                'message' => 'Error: Could not query the server, Invalid API Key maybe?',
            ];
        }

        $data = json_decode($request->getBody(), true);
        // var_dump($data);die;
        if (!isset($data['status']) || $data['status'] !== 'OK') {
            return [
                'status' => 400,
                'message' => 'Error 2: Location seems to be invalid, try again.'
            ];
        }

        $data['status'] = 200;
        return $data;
    }
}
