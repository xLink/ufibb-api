<?php

namespace App\Http\Controllers\Timezone;

use App\Http\Controllers\Controller;

class BaseTz extends Controller
{
    protected $commands = ['tz', 'timezone'];
}
