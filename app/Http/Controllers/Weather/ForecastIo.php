<?php

namespace App\Http\Controllers\Weather;

use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Http\Request;

class ForecastIo extends BaseWeather
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\Weather\ForecastIo';
    protected $version = '1.0.4';
    protected $services = ['forecastio'];

    protected $cache;
    protected $cacheKey;
    protected $cacheHit;

    public function run(Request $request, Cache $cache)
    {
        clock()->startEvent('forecastio', 'Start');
        $this->cache = $cache;
        $input = $request->all();

        // make sure we have something for this request
        if (substr(array_get($input, 'message.args.0'), 0, 1) == '?') {
            return $this->sendError('Usage: <location>', 200);
        }

        // check for weather info out of darchoods api
        if (array_get($input, 'message.arg_count', '0') == '0') {
            return $this->sendError('Usage: <location>. (Sorry you\'ll have to ask for location, I can\'t access the DH Api yet)', 400);
        }

        // try grab the long/lat for $location
        $location = str_replace(array_get($input, 'message.command').' ', '', array_get($input, 'message.text'));
        $this->cacheKey = 'longLat-' . $location;
        $longLat = $this->getGoogleLongLat($location);
        if (is_array($longLat) && !isset($longLat['results'])) {
            clock('issue with longLat, removing cache key');
            $this->cache->forget($this->cacheKey);
            return $this->sendError($longLat['message'], $longLat['status']);
        }

        // grab the actual forcast for the longLat
        $weather = $this->getForecastWeather($longLat, $request);
        if (is_array($weather) && isset($weather['status'])) {
            return $this->sendError($weather['message'], $weather['status']);
        }

        $return = $this->formatOutput($weather, $longLat, $request);
        clock()->endEvent('forecastio');
        return $this->sendResponse('ok', '200', $return);
    }

    private function formatOutput($weather, $location, Request $request)
    {
        $input = $request->all();
        $currently = array_get($weather, 'currently');

        $raw = [
            'location' => array_get($location, 'results.0.formatted_address'),
            'current-summary' => sprintf('%s', $currently['summary']),
            'hourly-summary' => sprintf('%s', array_get($weather, 'hourly.summary')),
            'temp-c' => sprintf('%d °C', round((($currently['temperature'] - 32) * 5) / 9)),
            'temp-f' => sprintf('%d °F', round($currently['temperature'])),
            'humidity' => sprintf('%d%%', $currently['humidity'] * 100),
            'wind-speed' => sprintf('%d mph', $currently['windSpeed']),
            'wind-direction' => sprintf('%d', $currently['windBearing']),
        ];

        return [
            'cacheHit' => $this->cacheHit,
            'raw' => $raw,
            'return' => [
                'to' => array_get($input, 'message.to'),
                'method' => 'privmsg',
                'message' => sprintf(
                    '[ %1$s | %2$s (%3$s) | Temp: %4$s | %5$s | Humidity: %6$s | Winds: %7$s ]',
                    array_get($raw, 'location'),
                    array_get($raw, 'current-summary'),
                    array_get($raw, 'hourly-summary'),
                    array_get($raw, 'temp-c'),
                    array_get($raw, 'temp-f'),
                    array_get($raw, 'humidity'),
                    array_get($raw, 'wind-speed')
                ),
            ],
        ];
    }

    private function getGoogleLongLat($location)
    {
        if (empty($location)) {
            return [
                'status' => 411,
                'message' => 'Error: No Location Given.',
            ];
        }

        if ($this->cache->has($this->cacheKey)) {
            clock('got longLat returning');
            $this->cacheHit = true;
            return $this->cache->get($this->cacheKey);
        }
        clock('no longLat data');
        $this->cacheHit = false;

        $url = sprintf('http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false', urlencode($location));
        clock('Pinging', $url);
        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error 1: Could not query the server.',
            ];
        }

        $gAPI = json_decode($request->getBody(), true);
        if (!count($gAPI) || array_get($gAPI, 'results.0', null) === null) {
            return [
                'status' => 400,
                'message' => 'Error 2: Location seems to be invalid, try again.',
            ];
        }
        $this->cache->rememberForever($this->cacheKey, function () use ($gAPI) {
            clock('adding longLat to cache');
            return $gAPI;
        });

        return $gAPI;
    }

    private function getForecastWeather($location, Request $request)
    {
        $input = $request->all();
        if (!is_array($location)) {
            return [
                'status' => 411,
                'message' => 'Error 4: Could not get long/lat for location.',
            ];
        }

        $longLat = array_get($location, 'results.0.geometry.location');
        $longLat = (string) $longLat['lat'] . ',' . $longLat['lng'];

        $url = sprintf('https://api.forecast.io/forecast/%s/%s', array_get($input, 'services.forecastio'), $longLat);
        clock('Pinging', $url);
        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 411,
                'message' => 'Error 5: Could not query the server, Invalid API Key maybe?',
            ];
        }

        $forecast = json_decode($request->getBody(), true);
        if (!isset($forecast['currently'])) {
            return [
                'status' => 400,
                'message' => 'Error 6: Location seems to be invalid, try again.',
            ];
        }

        return $forecast;
    }
}
