<?php

namespace App\Http\Controllers\Weather;

use App\Http\Controllers\Controller;

class BaseWeather extends Controller
{
    protected $commands = ['w', 'weather'];
}
