<?php

namespace App\Http\Controllers\NowPlaying;

use App\Http\Controllers\Controller;

class BaseNP extends Controller
{
    protected $commands = ['np'];
}
