<?php

namespace App\Http\Controllers\NowPlaying;

use Illuminate\Http\Request;

class Darkscience extends BaseNP
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\NowPlaying\Darkscience';
    protected $version = '1.1.2';
    protected $services = [''];

    public function run(Request $request)
    {
        $input = $request->all();
        $data = $this->getData();
        // var_dump($data);exit;
        $return = [
            'track' => [
                'artist' => array_get($data, 'icestats.source.artist'),
                'title' => array_get($data, 'icestats.source.title'),
                'bitrate' => array_get($data, 'icestats.source.audio_bitrate', 0),
            ],
            'stream' => [
                'status' => array_get($data, 'icestats.source.listeners', null) === null ? 'offline' : 'online',
                'listeners' => array_get($data, 'icestats.source.listeners', 0),
                'listener_peak' => array_get($data, 'icestats.source.listener_peak', 0),
                'stream_start' => \Carbon\Carbon::parse(array_get($data, 'icestats.source.stream_start'))->format('U'),
            ],
        ];

        return $this->sendResponse('ok', '200', [
            'raw' => $return,
            'return' => [
                'to' => array_get($input, 'message.to'),
                'method' => 'privmsg',
                'message' => array_get($return, 'stream.status', 'offline') === 'offline'
                ? '[ DS Radio | Stream is Offline ]'
                : sprintf(
                    '[ DS Radio | Track: %1$s - %2$s ]',
                    array_get($return, 'track.artist'),
                    array_get($return, 'track.title')
                ),
            ],
	    //'data' => $data,
        ]);
    }

    private function getData()
    {
        $url = 'http://radio.darkscience.net:8000/status-json.xsl';
        // $url = 'http://192.168.1.3:8000/status-json.xsl';

        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error 1: Could not query the server.',
                'request' => $request,
            ];
        }

        if ($request->getStatusCode() != '200') {
            return [
                'status' => 400,
                'message' => 'Error 2: Radio Service appears to be down, try again later.',
            ];
        }

        $body = $request->getBody();
	if (substr($body, -3) === ',}}') {
	    $body = str_replace(',}}', '}}}', $body);
	}

	$json = json_decode($body, true);
	if (($jsonError = json_last_error()) !== JSON_ERROR_NONE) {
	    return [
		'status' => 400,
		'message' => 'Error 3: Json Parsing Failed... ' . $jsonError,
		//'body' => $body,
	   ];
	}

	return $json; 
    }
}
