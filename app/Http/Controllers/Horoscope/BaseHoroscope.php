<?php

namespace App\Http\Controllers\Horoscope;

use App\Http\Controllers\Controller;

class BaseHoroscope extends Controller
{
    protected $commands = ['hs', 'horoscope'];
}
