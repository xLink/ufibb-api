<?php

namespace App\Http\Controllers\Horoscope;

use Illuminate\Http\Request;

class Horoscope extends BaseHoroscope
{
    protected $author = 'xLink';
    protected $class = 'App\Http\Controllers\Horoscope\Horoscope';
    protected $version = '1.0.0';
    protected $services = [];

    protected $signs = [
        'aries',
        'taurus',
        'gemini',
        'cancer',
        'leo',
        'virgo',
        'libra',
        'scorpio',
        'sagittarius',
        'capricorn',
        'aquarius',
        'pisces'
    ];

    public function run()
    {
        if (array_get($this->request, 'message.arg_count', '0') == '0' ||
            substr(array_get($this->request, 'message.args.0'), 0, 1) == '?') {
            return $this->sendResponse('Usage: ('.implode('|', $this->signs).')', 200);
        }

        $data = $this->getData(array_get($this->request, 'message.args.0'));
        if (is_array($data) && isset($data['status'])) {
            return $this->sendResponse($data['message'], $data['status']);
        }

        if (empty($data)) {
            $data = [
                'Error: No Results Found.',
            ];
        }

        return $this->sendResponse('ok', '200', [
            'raw' => $data,
            'return' => [
                'to' => array_get($this->request, 'message.to'),
                'method' => 'privmsg',
                'message' => $data,
            ],
        ]);
    }

    private function getData($sign)
    {
        if (!in_array($sign, $this->signs)) {
            return [
                'status' => 400,
                'message' => 'Error: Could not find the sign you were looking for...',
            ];
        }

        $url = sprintf('http://horoscope-api.herokuapp.com/horoscope/today/%s', strtolower($sign));

        // grab the request
        $request = guzzle('get', $url);
        if (($request instanceof \GuzzleHttp\Psr7\Response) === false) {
            return [
                'status' => 400,
                'message' => 'Error: Could not query the server. // 73',
            ];
        }

        if ($request->getStatusCode() != '200') {
            return [
                'status' => 400,
                'message' => 'Error: Request unsuccessful.',
            ];
        }

        $data = json_decode($request->getBody(), true);
        // echo var_dump($data); die;
        $hs = array_get($data, 'horoscope', false);
        if ($hs === false) {
            return [
                'status' => 500,
                'message' => 'Error: Could not get horoscope, please try again later.',
            ];
        }

        return $hs;
    }
}
